export const onSale = (item) => {
    if (item.regularPrice && item.salePrice) {
        if (item.price === item.regularPrice) {
            return false;
        }
        if (item.price === item.salePrice) {
            return true;
        } else {
            console.error("Invalid Item Price");
        }
    }
};
