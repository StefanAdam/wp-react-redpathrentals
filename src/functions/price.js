export const modifyTotals = (cart) => {
    let currentTotal = cart.total;
    let discountType = cart.coupons.discount_type;

    switch (discountType) {
        case "percent":
            let percentDiscount = cart.total * (1 - cart.coupons.amount / 100);
            if (percentDiscount < 0) {
                return 0.0;
            }
            return percentDiscount.toFixed(2);
        case "fixed_cart":
            let fixedDiscount = cart.total - cart.coupons.amount;
            if (fixedDiscount < 0) {
                return 0.0;
            }
            return fixedDiscount.toFixed(2);
        default:
            return currentTotal;
    }
};
