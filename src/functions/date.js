export const modifyDate = (date = new Date(), sign, count) => {
    switch (sign) {
        case "sub": {
            let subdate = new Date(date.setDate(date.getDate() - count));
            if (subdate.getDay() === 6) {
                let endDate = subdate.setDate(subdate.getDate() + 2);
                return endDate;
            }
            if (subdate.getDay() === 0) {
                let endDate = subdate.setDate(subdate.getDate() + 1);
                return endDate;
            }
            return subdate;
        }
        case "add": {
            let adddate = new Date(date.setDate(date.getDate() + count));
            if (adddate.getDay() === 6) {
                let endDate = adddate.setDate(adddate.getDate() + 2);
                return endDate;
            }
            if (adddate.getDay() === 0) {
                let endDate = adddate.setDate(adddate.getDate() + 1);
                return endDate;
            }
            return adddate;
        }
        default:
            return date;
    }
};
