import _ from "lodash";

import { onSale } from "./sale";
import NotificationToast from "../components/NotificationToast/NotificationToast";

export const validCoupon = (coupon, cart) => {
    // #@coupon = object, @cart = object @return boolean
    // Coupon should be the object of the coupon being applied,
    // Cart is the current state object cart, contains items, an array of objects added to cart, coupons, an array of objects of applied coupons,
    // and total, a float describing the sum of the price of each item multipied by its quantity.
    // Function checks if coupon meets various criteria against details in cart, if coupon fails any validation step, function generates Error Toast and returns immediately.
    if (coupon === undefined) {
        // If passed an empty coupon object, return false.
        return false;
    }
    if (coupon.date_expires !== null) {
        let now = new Date();
        let expiry = Date.parse(coupon.date_expires);

        if (expiry < now) {
            NotificationToast(
                "Coupon Usage Restriction: Coupon is Expired.",
                "error"
            );
            return false;
        }
    }
    if (coupon.usage_limit !== null) {
        if (parseInt(coupon.usage_limit) >= parseInt(coupon.usage_count)) {
            NotificationToast(
                "Coupon Usage Restriction: Coupon Usage Limit Reached",
                "error"
            );
            return;
        }
    }
    if (coupon.minimum_amount > 0) {
        // If coupon has minimum_amount, and cart is less than it, return false.
        if (parseFloat(cart.total) < parseFloat(coupon.minimum_amount)) {
            NotificationToast(
                "Coupon Usage Restriction: Minimum Value not Reached",
                "error"
            );
            return false;
        }
    }
    if (coupon.maximum_amount > 0) {
        // If coupon has maximum_amount, and cart exceeds it, return false
        if (parseFloat(cart.total) > parseFloat(coupon.maximum_amount)) {
            NotificationToast(
                "Coupon Usage Restriction: Maximum Value of Cart Exceeded",
                "error"
            );
            return false;
        }
    }
    if (coupon.allowed_item_ids.length > 0) {
        // If coupon has allowed_item_ids, and cart.items does not have an item that matches, return false
        // coupon.allowed_items_ids is an array of zero - many integers.
        // cart.items is an array of zero - many objects, with key id containing an ID.
        // if any of cart.items[?].id is not equal to any of coupon.allowed_items_ids,
        // then anerror toast should be sent and activateCoupon should return immediately.
        if (cart.items.length <= 0) {
            NotificationToast(
                "Coupon Usage Restriction: Required Item not in Cart",
                "error"
            );
            return false;
        }
        let match = cart.items.some((item) => {
            return coupon.allowed_item_ids.includes(item.id);
        });
        if (match === false) {
            NotificationToast(
                "Coupon Usage Restriction: Required Item not in Cart",
                "error"
            );
            return false;
        }
    }
    if (coupon.excluded_item_ids.length > 0) {
        // If coupon has excluded_item_ids, and cart.items has an item that matches, return false
        // coupon.allowed_items_ids is an array of zero - many integers.
        // cart.items is an array of zero - many objects, with key id containing an ID.
        // if any of cart.items[?].id is equal to any of coupon.allowed_items_ids, then coupons should not be applied,
        let match = cart.items.some((item) => {
            return coupon.excluded_item_ids.includes(item.id);
        });
        if (match === true) {
            NotificationToast(
                "Coupon Usage Restriction: Blacklisted Item in Cart",
                "error"
            );
            return false;
        }
    }

    if (coupon.product_categories.length > 0) {
        let match = cart.items.some((item) => {
            return item.categories.some((category) => {
                return coupon.product_categories.includes(category.id);
            });
        });
        if (match !== true) {
            NotificationToast(
                "Coupon Usage Restriction: No Items in Cart of Required Category",
                "error"
            );
            return false;
        }
    }
    if (coupon.excluded_product_categories.length > 0) {
        let match = cart.items.some((item) => {
            return item.categories.some((category) => {
                return coupon.excluded_product_categories.includes(category.id);
            });
        });
        if (match === true) {
            NotificationToast(
                "Coupon Usage Restriction: Blacklisted Category in Cart",
                "error"
            );
            return false;
        }
    }
    if (coupon.exclude_sale_items === true) {
        let saleItemInCart = cart.items.some((item) => {
            if (item.salePrice !== undefined && item.salePrice !== "") {
                let valid = onSale(item);
                return valid;
            }
            return false;
        });
        if (saleItemInCart === true) {
            NotificationToast(
                "Coupon Usage Restriction: Coupon cannot be combined with Sale Item",
                "error"
            );
            return false;
        }
    }

    return true; //If no disqualifiers are detected, return true.
};

export const activateCoupon = (code, coupons, cart, applyCoupon) => {
    // #@code = string, @coupon = object, @cart = object, @applyCoupon = function, @cart = object.
    // code is string recieved from User
    // coupons is array of coupons fetched from WP Backend.
    // cart is a state object containing items: an array of products added to cart, coupons: an object with the currently applied coupon, total: a string containing total price of all elements in items, multiplied by their quantity.
    // applyCoupon is a Redux Action that fires the APPLY_COUPON event in Redux.

    const couponArray = Object.values(coupons).map((coupon, index) => {
        // Creates an array that checks code against every item in coupons, consisting of the matching coupon, and null entries.
        if (coupon.code === code) {
            return coupon;
        }
        return null;
    });

    _.remove(couponArray, (element) => {
        // mutates couponArray to remove all items except coupon that matches code.
        return element === null;
    });

    const [coupon] = couponArray; // Destructures coupon from couponArray. This will either be undefined, or an object.

    if (coupon === undefined) {
        NotificationToast("Invalid Coupon Code", "error");
    }

    const valid = validCoupon(coupon, cart); // Sends matching coupon to validCoupon to check if coupon is allowed for current cart. validCoupon returns a boolean. true, add coupon, false, do nothing.
    if (valid) {
        NotificationToast("Coupon Added", "sucess");
        applyCoupon(coupon);
    }
};
