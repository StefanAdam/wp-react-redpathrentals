import NotificationToast from "../components/NotificationToast/NotificationToast";

export const checkValid = (
    value,
    label,
    type,
    restrictions = [],
    validate = true
) => {
    if (validate === false) {
        return { [label]: { valid: null } };
    }
    switch (type) {
        case "text": {
            let end =
                value === ""
                    ? { [label]: { valid: false } }
                    : { [label]: { valid: true } };

            if (end[label].valid === false) {
                let message = `${label}: Cannot be blank`;
                NotificationToast(message, "error");
            }

            return end;
        }
        case "select": {
            let end =
                value === "select"
                    ? { [label]: { valid: false } }
                    : { [label]: { valid: true } };
            if (end[label].valid === false) {
                let message = `${label}: Cannot be default`;
                NotificationToast(message, "error");
            }

            return end;
        }
        case "postalcode": {
            if (value !== "") {
                let regex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
                let match = regex.exec(value);
                if (match) {
                    let digits = value.slice(0, 3);
                    let end = restrictions.validPostalCodes.includes(digits);
                    if (end === true) {
                        return { [label]: { valid: true } };
                    }
                    let message = `${label}: Outside of Service Area`;
                    NotificationToast(message, "error");
                    return { [label]: { valid: false } };
                }
                let message = `${label}: Invalid Postal Format`;
                NotificationToast(message, "error");
                return { [label]: { valid: false } };
            }
            let message = `${label}: Cannot be blank`;
            NotificationToast(message, "error");
            return { [label]: { valid: false } };
        }
        case "checkbox": {
            console.log(value);
            let end =
                value === true
                    ? { [label]: { valid: true } }
                    : { [label]: { valid: false } };

            if (end[label].valid === false) {
                let message = `${label}: Must be accepted`;
                NotificationToast(message, "error");
            }

            return end;
        }
        default:
            throw new Error("Unexpected Output in checkValid");
    }
};

export const displayError = (value) => {
    return value !== null ? (value ? "" : "error") : "";
};
