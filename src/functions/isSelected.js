const isSelected = (
    comparison = "selected",
    id,
    success = "active",
    fail = ""
) => {
    return comparison === id ? success : fail;
};

export default isSelected;
