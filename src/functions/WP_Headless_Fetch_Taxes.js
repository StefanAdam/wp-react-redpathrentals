import woocommerce from "../apis/woocommerce";

const WP_Headless_Fetch_Taxes = async (setTaxes, per_page = 100) => {
    const Taxes = await woocommerce.get("taxes/", {
        per_page,
    });

    const TaxArray = Taxes.data.map((tax) => {
        return {
            id: tax.id,
            country: tax.country,
            state: tax.state,
            postcode: tax.postcode,
            city: tax.city,
            rate: tax.rate,
            priority: tax.priority,
            compound: tax.compound,
            shipping: tax.shipping,
            class: tax.class,
        };
    });
    setTaxes(TaxArray);
};

export default WP_Headless_Fetch_Taxes;
