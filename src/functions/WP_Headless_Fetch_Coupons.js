import woocommerce from "../apis/woocommerce";

const WP_Headless_Fetch_Coupons = async (setCoupons, per_page, code) => {
    const Coupons = await woocommerce.get("coupons/", {
        per_page,
    });

    const CouponArray = Coupons.data.map((coupon) => {
        return {
            code: coupon.code,
            id: coupon.id,
            amount: coupon.amount,
            discount_type: coupon.discount_type,
            description: coupon.description,
            date_expires: coupon.date_expires,
            usage_count: coupon.usage_count,
            usage_limit: coupon.usage_limit,
            allowed_item_ids: coupon.product_ids,
            excluded_item_ids: coupon.excluded_product_ids,
            free_shipping: coupon.free_shipping,
            product_categories: coupon.product_categories,
            excluded_product_categories: coupon.excluded_product_categories,
            exclude_sale_items: coupon.exclude_sale_items,
            minimum_amount: coupon.minimum_amount,
            maximum_amount: coupon.maximum_amount,
        };
    });
    setCoupons(CouponArray);
};

export default WP_Headless_Fetch_Coupons;
