import woocommerce from "../apis/woocommerce";

const WP_Headless_Fetch_Products = async (setProducts, per_page, category) => {
    const Products = await woocommerce.get("products/", {
        per_page,
        category,
    });

    const ProductArray = Products.data.map((product) => {
        const price = {
            blockPrice: product.meta_data.find(
                (item) => item.key === "rental_block_price"
            ),
            basePrice: product.meta_data.find(
                (item) => item.key === "rental_base_price"
            ),
        };
        const quantity = [];
        if (
            product.meta_data.find(
                (item) => item.key === "_isa_wc_max_qty_product_max"
            )
        ) {
            quantity.push(
                product.meta_data.find(
                    (item) => item.key === "_isa_wc_max_qty_product_max"
                )
            );
        } else {
            quantity.push({ value: null });
        }

        return {
            name: product.name,
            id: product.id,
            description: product.description,
            categories: product.categories,
            basePrice: price.basePrice.value,
            blockPrice: price.blockPrice.value,
            purchasable: product.purchasable,
            slug: product.slug,
            image: product.images[0].src,
            price: product.price,
            maxQuantity: quantity[0].value,
            salePrice: product.sale_price,
            regularPrice: product.regular_price,
            saleStart: product.date_on_sale_from,
            saleEnd: product.date_on_sale_to,
        };
    });
    setProducts(ProductArray);
};

export default WP_Headless_Fetch_Products;
