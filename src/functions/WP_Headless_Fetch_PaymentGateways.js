import woocommerce from "../apis/woocommerce";

const WP_Headless_Fetch_PaymentGateways = async () => {
    const Products = await woocommerce.get("payment_gateways/");

    console.log(Products);
};

export default WP_Headless_Fetch_PaymentGateways;
