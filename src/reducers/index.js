import { combineReducers } from "redux";

import pageReducer from "./pageReducer";
import rentalTypeReducer from "./rentalTypeReducer";
import productReducer from "./productReducer";
import currentDurationProductReducer from "./rentalInfoReducer";
import cartReducer from "./cartReducer";
import couponsReducer from "./couponsReducer";
import orderReducer from "./orderReducer";
import restrictionsReducer from "./restrictionsReducers";
import taxReducer from "./taxReducer";

export default combineReducers({
    page: pageReducer,
    rentalType: rentalTypeReducer,
    products: productReducer,
    rentalDetails: currentDurationProductReducer,
    cart: cartReducer,
    coupons: couponsReducer,
    order: orderReducer,
    restrictions: restrictionsReducer,
    taxes: taxReducer,
});
