export default (state = {}, action) => {
    switch (action.type) {
        case "ADD_COUPONS":
            let object = action.payload.reduce((obj, item) => {
                obj[item.id] = item;
                return obj;
            }, {});
            return { ...state, ...object };

        case "SET_COUPONS":
            let object1 = action.payload.reduce((obj, item) => {
                obj[item.id] = item;
                return obj;
            }, {});
            return { ...object1 };

        case "EMPTY_COUPONS":
            let object2 = {};
            return { ...object2 };

        default:
            return state;
    }
};
