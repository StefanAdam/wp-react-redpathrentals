const INITIAL_STATE = {
	currentRentalType: "residential",
	currentServiceType: "boxes"
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case "SET_RENTALTYPE":
			//console.log(action.payload);
			return { ...state, currentRentalType: action.payload };
		case "GET_RENTALTYPE":
			return state;
		case "SET_SERVICETYPE":
			return { ...state, currentServiceType: action.payload };
		case "GET_SERVICETYPE":
			return state;
		default:
			return state;
	}
};
