import { validCoupon } from "../functions/coupons";
import { cartDevState } from "./devStates/cartDevState";
import { setItemsCart } from "../actions";

const INITIAL_STATE = { items: [], coupons: [] };
const DEV_STATE = cartDevState;
export default (state = DEV_STATE, action) => {
    switch (action.type) {
        case "ADD_ITEM_CART":
            let elementID = action.payload[0].id;
            let inCart = { value: false };
            let couponValidAfterAdd = [];
            state.items.forEach((element, index) => {
                if (elementID === element.id) {
                    inCart.value = index;
                }
            });

            if (inCart.value !== false) {
                let Array3 = [...state.items];
                Array3[inCart.value].count = Array3[inCart.value].count + 1;

                if (state.coupons.id) {
                    let tempState = { ...state, items: Array3 };
                    couponValidAfterAdd.push(
                        validCoupon(state.coupons, tempState)
                    );
                }
                let returnCoupons =
                    couponValidAfterAdd[0] === true ? state.coupons : {};
                console.log(couponValidAfterAdd);
                return { ...state, items: Array3, coupons: returnCoupons };
            } else {
                let newItem = [...action.payload];
                newItem[0].count = 1;
                let Array3 = [...state.items, ...newItem];
                if (state.coupons.id) {
                    let tempState = { ...state, items: Array3 };
                    couponValidAfterAdd.push(
                        validCoupon(state.coupons, tempState)
                    );
                }
                let returnCoupons =
                    couponValidAfterAdd[0] === true ? state.coupons : {};
                console.log(couponValidAfterAdd);
                return { ...state, items: Array3, coupons: returnCoupons };
            }
        case "UPDATE_ITEM_CART":
            let elementID1 = action.payload.item[0].id;
            let count = { value: action.payload.value };
            let max = action.payload.item[0].maxQuantity;
            let inCart1 = { value: false };
            state.items.forEach((element, index) => {
                if (elementID1 === element.id) {
                    inCart1.value = index;
                }
            });
            if (inCart1.value !== false) {
                let Array3 = [...state.items];
                if (max !== null) {
                    if (parseInt(count.value) > parseInt(max)) {
                        count.value = max;
                    }
                }
                Array3[inCart1.value].count = count.value;
                if (Array3[inCart1.value].count <= 0) {
                    Array3.splice(inCart1.value, 1);
                }
                return { ...state, items: Array3 };
            }
            return state;

        case "REMOVE_ITEM_CART":
            let elementID2 = action.payload[0].id;
            let inCart2 = { value: false };
            let couponValidAfterRemove = [];

            state.items.forEach((element, index) => {
                if (elementID2 === element.id) {
                    inCart2.value = index;
                }
            });

            if (inCart2.value !== false) {
                let Array3 = [...state.items];
                Array3.splice(inCart2.value, 1);
                if (state.coupons.id) {
                    let tempState = { ...state, items: Array3 };
                    couponValidAfterRemove.push(
                        validCoupon(state.coupons, tempState)
                    );
                }
                let returnCoupons =
                    couponValidAfterRemove[0] === true ? state.coupons : {};
                return { ...state, items: Array3, coupons: returnCoupons };
            }
            return state;

        case "APPLY_COUPON":
            return { ...state, coupons: action.payload };

        case "REMOVE_COUPON":
            let object1 = action.payload.reduce((obj, item) => {
                obj[item.id] = item;
                return obj;
            }, {});
            return { ...object1 };

        case "SET_TOTAL":
            let total = action.payload;
            return { ...state, total };

        default:
            return state;
    }
};
