export default (state = {}, action) => {
    switch (action.type) {
        case "ADD_PRODUCT":
            return { ...state, [action.payload.id]: action.payload };

        case "ADD_PRODUCTS":
            let object = action.payload.reduce((obj, item) => {
                obj[item.id] = item;
                return obj;
            }, {});
            return { ...state, ...object };

        case "SET_PRODUCTS":
            let object1 = action.payload.reduce((obj, item) => {
                obj[item.id] = item;
                return obj;
            }, {});
            return { ...object1 };

        case "EMPTY_PRODUCTS":
            let object2 = {};
            return { ...object2 };

        default:
            return state;
    }
};
