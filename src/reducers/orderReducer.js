import { modifyDate } from "../functions/date";
import { orderDevState } from "./devStates/orderDevState";
const INITIAL_STATE = {
    delivery: {
        country: { value: "CA", valid: true },
        province: { value: "BC", valid: true },
        street_address: { value: "", valid: null },
        street_address_2: { value: "", valid: true },
        city: { value: "", valid: null },
        postal_code: { value: "", valid: null },
        date: { value: modifyDate(new Date(), "add", 2), valid: null },
        time: { value: "", valid: null },
        notes: { value: "", valid: null },
    },
    pickup: {
        country: { value: "CA", valid: true },
        province: { value: "BC", valid: true },
        street_address: { value: "", valid: null },
        street_address_2: { value: "", valid: true },
        city: { value: "", valid: null },
        postal_code: { value: "", valid: null },
        date: { value: "", valid: null },
        time: { value: "", valid: null },
        notes: { value: "", valid: null },
    },
    billing: {
        first_name: { value: "", valid: null },
        last_name: { value: "", valid: null },
        country: { value: "", valid: null },
        street_address: { value: "", valid: null },
        street_address_2: { value: "", valid: true },
        city: { value: "", valid: null },
        postal_code: { value: "", valid: null },
        province: { value: "", valid: null },
        phone: { value: "", valid: null },
        email: { value: "", valid: null },
        terms: { value: "", valid: null },
    },
};

const DEV_STATE = orderDevState;

export default (state = DEV_STATE, action) => {
    switch (action.type) {
        case "UPDATE_DELIVERY": {
            let delivery = action.payload;

            return { ...state, delivery: { ...delivery } };
        }
        case "UPDATE_PICKUP": {
            let pickup = action.payload;

            return { ...state, pickup: { ...pickup } };
        }
        case "UPDATE_BILLING": {
            let billing = action.payload;

            return { ...state, billing: { ...billing } };
        }
        case "SET_ORDER_TAX": {
            let tax = action.payload;
            return { ...state, tax };
        }
        case "SET_ORDER_TOTAL": {
            let total = action.payload;
            return { ...state, total };
        }
        default:
            return state;
    }
};
