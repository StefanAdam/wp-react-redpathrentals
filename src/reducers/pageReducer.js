const INITIAL_STATE = {
	currentPage: "rental-type"
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case "SET_CURRENTPAGE":
			//console.log(action.payload);
			return { ...state, currentPage: action.payload };
		default:
			return state;
	}
};
