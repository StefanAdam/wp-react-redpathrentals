import { rentalInfoDevState } from "./devStates/rentalInfoDevState";
const INITIAL_STATE = {
    currentDuration: "1",
    currentProduct: {},
    currentPrice: null,
};

const DEV_STATE = rentalInfoDevState;

export default (state = DEV_STATE, action) => {
    switch (action.type) {
        case "SET_CURRENT_DURATION":
            //console.log(action.payload);
            return { ...state, currentDuration: action.payload };
        case "GET_CURRENT_DURATION":
            return state;
        case "SET_CURRENT_PRODUCT":
            return { ...state, currentProduct: action.payload };
        case "GET_CURRENT_PRODUCT":
            return state;
        case "SET_CURRENT_PRICE":
            return { ...state, currentPrice: action.payload };
        case "GET_CURRENT_PRICE":
            return state;
        default:
            return state;
    }
};
