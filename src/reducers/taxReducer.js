export default (state = [], action) => {
    switch (action.type) {
        case "SET_TAXES":
            let setTaxes = action.payload;
            return [...setTaxes];

        default:
            return state;
    }
};
