export const orderDevState = {
    delivery: {
        country: { value: "CA", valid: true },
        province: { value: "BC", valid: true },
        street_address: {
            value: "7 Burbidge Street",
            valid: true,
        },
        street_address_2: {
            value: "Unit 102",
            valid: null,
        },
        city: {
            value: "Coquitlam",
            valid: true,
        },
        postal_code: {
            value: "V3K 7B2",
            valid: true,
        },
        date: {
            value: "2020-01-28T00:30:40.000Z",
            valid: true,
        },
        time: {
            value: "morning",
            valid: true,
        },
        notes: {
            value: "Delivery Notes!",
            valid: null,
        },
        value: "Delivery Notes!",
        valid: null,
    },
    pickup: {
        country: { value: "CA", valid: true },
        province: { value: "BC", valid: true },
        street_address: {
            value: "21555 Dewdney Trunk Road",
            valid: true,
        },
        street_address_2: {
            value: "19",
            valid: null,
        },
        city: {
            value: "Maple Ridge",
            valid: true,
        },
        postal_code: {
            value: "V2X 3G6",
            valid: true,
        },
        date: {
            value: "2020-02-24T08:00:00.000Z",
            valid: true,
        },
        time: {
            value: "morning",
            valid: true,
        },
        notes: {
            value: "Pickup Notes!",
            valid: null,
        },
        value: "Pickup Notes!",
        valid: null,
    },
    billing: {
        first_name: {
            value: "Stefan",
            valid: true,
        },
        last_name: {
            value: "Adam",
            valid: true,
        },
        country: {
            value: "CA",
            valid: true,
        },
        street_address: {
            value: "11083 Shaw St",
            valid: true,
        },
        street_address_2: {
            value: "#",
            valid: true,
        },
        city: {
            value: "Mission",
            valid: true,
        },
        postal_code: {
            value: "V4S 1B1",
            valid: null,
        },
        province: {
            value: "BC",
            valid: true,
        },
        phone: {
            value: "604-942-8486",
            valid: true,
        },
        email: {
            value: "stefan@longevitygraphics.com",
            valid: true,
        },
        terms: {
            value: true,
            valid: true,
        },
        value: true,
        valid: true,
    },
};
