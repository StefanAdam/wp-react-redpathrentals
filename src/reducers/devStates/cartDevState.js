export const cartDevState = {
    items: [
        {
            name: "Labels (10)",
            id: 711,
            description: "",
            categories: [
                {
                    id: 27,
                    name: "Addon",
                    slug: "addon",
                },
            ],
            basePrice: "",
            blockPrice: "",
            purchasable: true,
            slug: "labels",
            image:
                "http://redpathrentals.test/wp-content/uploads/2019/10/redpath-moving-labels-red.jpg",
            price: "2.50",
            maxQuantity: null,
            salePrice: "",
            regularPrice: "2.50",
            saleStart: null,
            saleEnd: null,
            count: 1,
        },
        {
            name: "Tape Gun/Dispenser",
            id: 713,
            description: "",
            categories: [
                {
                    id: 27,
                    name: "Addon",
                    slug: "addon",
                },
            ],
            basePrice: "",
            blockPrice: "",
            purchasable: true,
            slug: "tape",
            image:
                "http://redpathrentals.test/wp-content/uploads/2019/10/redpathrentals-tapedispenser.jpg",
            price: "4",
            maxQuantity: null,
            salePrice: "",
            regularPrice: "4",
            saleStart: null,
            saleEnd: null,
            count: 1,
        },
    ],
    coupons: {},
    total: "6.50",
};
