export const rentalInfoDevState = {
    currentDuration: "4",
    currentProduct: {
        name: "25 RED Boxes",
        id: 704,
        description:
            "<p><span>Included:</span><span></span></p>\n<ul>\n<li>25 RED Boxes</li>\n<li>25 Labels</li>\n<li>6 RED Wheeled Bases</li>\n</ul>\n",
        categories: [
            {
                id: 25,
                name: "Boxes",
                slug: "boxes",
            },
            {
                id: 28,
                name: "Package",
                slug: "package",
            },
        ],
        basePrice: "75",
        blockPrice: "25",
        purchasable: true,
        slug: "boxes-25",
        image:
            "http://redpathrentals.test/wp-content/uploads/2019/04/box-25.svg",
        price: "0",
        maxQuantity: null,
        salePrice: "",
        regularPrice: "0",
        saleStart: null,
        saleEnd: null,
    },
    currentPrice: 175,
};
