import React from "react";
import { StripeProvider, Elements, injectStripe } from "react-stripe-elements";
import MyStoreCheckout from "./MyStoreCheckout";

const StripeTest = () => {
    return (
        <StripeProvider apiKey="pk_test_kz71oq6wxLCCCq0Ud1VmCddS00HuOwrhrh">
            <MyStoreCheckout />
        </StripeProvider>
    );
};

export default StripeTest;
