export const setCurrentPage = (page) => {
    return {
        type: "SET_CURRENTPAGE",
        payload: page,
    };
};

export const setRentalType = (type) => {
    return {
        type: "SET_RENTALTYPE",
        payload: type,
    };
};

export const getRentalType = () => {
    return {
        type: "GET_RENTALTYPE",
    };
};

export const setServiceType = (type) => {
    return {
        type: "SET_SERVICETYPE",
        payload: type,
    };
};

export const getServiceType = () => {
    return {
        type: "GET_RENTALTYPE",
    };
};

export const addProduct = (productDetails) => {
    return {
        type: "ADD_PRODUCT",
        payload: productDetails,
    };
};

export const addProducts = (productDetails) => {
    return {
        type: "ADD_PRODUCTS",
        payload: productDetails,
    };
};

export const setProducts = (productDetails) => {
    return {
        type: "SET_PRODUCTS",
        payload: productDetails,
    };
};

export const emptyProducts = (productDetails) => {
    return {
        type: "EMPTY_PRODUCTS",
    };
};

export const setCurrentDuration = (type) => {
    return {
        type: "SET_CURRENT_DURATION",
        payload: type,
    };
};

export const getDuration = () => {
    return {
        type: "GET_CURRENT_DURATION",
    };
};

export const setCurrentProduct = (type) => {
    return {
        type: "SET_CURRENT_PRODUCT",
        payload: type,
    };
};

export const getCurrentProduct = () => {
    return {
        type: "GET_CURRENT_PRODUCT",
    };
};

export const setCurrentPrice = (type) => {
    return {
        type: "SET_CURRENT_PRICE",
        payload: type,
    };
};

export const getCurrentPrice = () => {
    return {
        type: "GET_CURRENT_PRICE",
    };
};

export const addItemCart = (item) => {
    return {
        type: "ADD_ITEM_CART",
        payload: item,
    };
};

export const updateItemCart = (item, value) => {
    return {
        type: "UPDATE_ITEM_CART",
        payload: { item, value },
    };
};

export const removeItemCart = (item) => {
    return {
        type: "REMOVE_ITEM_CART",
        payload: item,
    };
};

export const addCoupons = (productDetails) => {
    return {
        type: "ADD_COUPONS",
        payload: productDetails,
    };
};

export const setCoupons = (coupons) => {
    return {
        type: "SET_COUPONS",
        payload: coupons,
    };
};

export const emptyCoupons = (coupons) => {
    return {
        type: "EMPTY_COUPONS",
        payload: coupons,
    };
};

export const applyCoupon = (coupons) => {
    return {
        type: "APPLY_COUPON",
        payload: coupons,
    };
};

export const removeCoupon = (coupons) => {
    return {
        type: "REMOVE_COUPON",
        payload: coupons,
    };
};

export const setTotal = (total) => {
    return {
        type: "SET_TOTAL",
        payload: total,
    };
};

export const updateDelivery = (delivery) => {
    return {
        type: "UPDATE_DELIVERY",
        payload: delivery,
    };
};

export const updatePickup = (pickup) => {
    return {
        type: "UPDATE_PICKUP",
        payload: pickup,
    };
};

export const updateBilling = (billing) => {
    return {
        type: "UPDATE_BILLING",
        payload: billing,
    };
};

export const setOrderTax = (tax) => {
    return {
        type: "SET_ORDER_TAX",
        payload: tax,
    };
};

export const setOrderTotal = (total) => {
    return {
        type: "SET_ORDER_TOTAL",
        payload: total,
    };
};

export const setTaxes = (taxes) => {
    return {
        type: "SET_TAXES",
        payload: taxes,
    };
};
