import React, { useEffect } from "react"; // eslint-disable-line no-unused-vars
import { Router, Switch, Route } from "react-router-dom";

import history from "../history";
import {
    ToastsContainer,
    ToastsContainerPosition,
    ToastsStore,
} from "@vashnak/react-toasts";

import ProgressTracker from "./ProgressBar/ProgressTracker";

import RentalTypeTab from "./tabs/RentalTypeTab/RentalTypeTab";
import ServiceTypeTab from "./tabs/ServiceTypeTab";
import SelectionTab from "./tabs/SelectionTab/SelectionTab";
import AddonsTab from "./tabs/AddonsTab/AddonsTab";
import DeliveryTab from "./tabs/DeliveryTab/DeliveryTab";
import PickupTab from "./tabs/PickupTab/PickupTab";
import BillingTab from "./tabs/BillingTab/BillingTab";
import ReviewTab from "./tabs/ReviewTab/ReviewTab";

const App = () => {
    // useEffect(() => {
    //     history.push("/rental-type");
    // }, []);
    return (
        <div className="container">
            <ProgressTracker />
            <ToastsContainer
                position={ToastsContainerPosition.BOTTOM_CENTER}
                store={ToastsStore}
                timer="8000"
            />
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={RentalTypeTab} />
                    <Route
                        path="/rental-type"
                        exact
                        component={RentalTypeTab}
                    />
                    <Route
                        path="/service-type"
                        exact
                        component={ServiceTypeTab}
                    />
                    <Route path="/selection" exact component={SelectionTab} />
                    <Route path="/addons" exact component={AddonsTab} />
                    <Route path="/delivery" exact component={DeliveryTab} />
                    <Route path="/pickup" exact component={PickupTab} />
                    <Route path="/billing" exact component={BillingTab} />
                    <Route path="/review" exact component={ReviewTab} />
                </Switch>
            </Router>
        </div>
    );
};

export default App;

///// TODO LIST
//// Validation
/// Prevent progress when validation-required feeds have not been completed
/// FIX Billing Validation
// Street Address 2 should not be required
// Phone needs validation
// email needs validation
// terms state failing
