import React from "react";

const SelectionSummary = (props) => {
    const { rentalDetails, description, text, image } = props;
    const { currentProduct, currentDuration, currentPrice } = rentalDetails;
    return (
        <>
            {!isNaN(currentProduct.blockPrice) && (
                <div className="sel-summary">
                    <div className="sel-summary__content">
                        <h3 className="sel-summary__text">
                            {`${text} ${
                                currentProduct.name
                            } for ${currentDuration} Week${
                                parseInt(currentDuration) > 1 ? "s" : ""
                            }`}
                        </h3>
                        <h3 className="sel-summary__price">{`$${currentPrice}`}</h3>
                    </div>

                    {image && (
                        <div className="sel-summary__image">
                            <img
                                src={currentProduct.image}
                                alt={currentProduct.name}
                            />
                        </div>
                    )}

                    {description && (
                        <p
                            className="sel-summary__description"
                            dangerouslySetInnerHTML={{
                                __html: currentProduct.description,
                            }}
                        />
                    )}
                </div>
            )}
        </>
    );
};

export default SelectionSummary;
