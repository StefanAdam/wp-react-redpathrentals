import React, { useEffect } from "react";
import { connect } from "react-redux";

import { setProducts, setCurrentPrice } from "../../../actions";

import WP_Headless_Fetch_Products from "../../../functions/WP_Headless_Fetch_Products";

import DurationSlider from "./DurationSlider";
import ProductSlider from "../ProductSlider/ProductSlider";
import SelectionSummary from "./SelectionSummary";
import NavigationBox from "../../NavigationBox";

import "./SelectionSummary.scss";

const updatePrice = (setCurrentPrice, product, duration) => {
    setCurrentPrice(
        parseInt(product.basePrice) +
            parseInt(product.blockPrice) * parseInt(duration)
    );
};

const SelectionTab = (props) => {
    const { setProducts, rentalDetails } = props;
    const { currentDuration, currentProduct, currentPrice } = rentalDetails;

    // Only Runs once when Component Mounts // Effectively ComponentDidMount
    useEffect(() => {
        WP_Headless_Fetch_Products(setProducts, 100, 25);
    }, [setProducts]);

    // Runs when any of the Duration, Selected Product or Price Changes. // Effectively ComponentDidUpdate
    useEffect(() => {}, []);
    return (
        <div>
            <h1> Select your rental duration and the quantity of RED Boxes </h1>
            <DurationSlider duration={currentDuration}></DurationSlider>
            <ProductSlider products={props.products} slides="5" />
            <SelectionSummary
                rentalDetails={rentalDetails}
                text="Your Selection: "
                description
            />
            <NavigationBox back="/service-type" forward="/addons" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        rentalDetails: state.rentalDetails,
        currentServiceType: state.rentalType.currentServiceType,
        products: state.products,
    };
};

export default connect(mapStateToProps, {
    setProducts,
    setCurrentPrice,
})(SelectionTab);
