import React from "react";
import { connect } from "react-redux";

import { setCurrentDuration } from "../../../actions";

const createSlider = (duration) => {
    let sliderContents = [];
    for (let x = 1; x < 5; x++) {
        sliderContents.push(
            <div
                id={x}
                key={x}
                className={`btn btn-outline-dark ${
                    parseInt(duration) === x ? "active" : " "
                }`}
            >
                {x} Week{x > 1 ? "s" : ""}
            </div>
        );
    }
    return sliderContents;
};

const DurationSlider = (props) => {
    const { setCurrentDuration, duration } = props;
    return (
        <div
            onClick={(e) => {
                if (
                    !isNaN(parseInt(e.target.id)) &&
                    isFinite(parseInt(e.target.id))
                ) {
                    setCurrentDuration(e.target.id);
                }
            }}
        >
            {createSlider(duration)}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        currentDuration: state.rentalDetails.currentDuration,
    };
};

export default connect(mapStateToProps, {
    setCurrentDuration,
})(DurationSlider);
