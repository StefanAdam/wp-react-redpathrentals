import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import NavigationBox from "../NavigationBox";

import { setServiceType, getServiceType } from "../../actions";
import isSelected from "../../functions/isSelected";

import "./RentalTypeTab/RentalTypeTab.scss";

import { ReactComponent as BoxSVG } from "./Assets/boxes.svg";
import { ReactComponent as PackageSVG } from "./Assets/packages.svg";

const ServiceTypeTab = (props) => {
    const [selected, setSelected] = useState(props.currentServiceType);

    useEffect(() => {
        setSelected(props.currentServiceType);
    }, [props.currentServiceType]);

    return (
        <div className="d-flex flex-column align-items-center">
            <h1>Select Service</h1>
            <p>I would Like:</p>
            <div className="two-panel d-flex justify-content-center">
                <div
                    className={`boxes service d-flex flex-column align-items-center ${isSelected(
                        selected,
                        "boxes",
                        "selected"
                    )}`}
                >
                    <BoxSVG onClick={() => props.setServiceType("boxes")} />
                    <span className="font-weight-bold">Boxes</span>
                </div>
                <div
                    className={`packages service d-flex flex-column align-items-center ${isSelected(
                        selected,
                        "package",
                        "selected"
                    )}`}
                >
                    <PackageSVG
                        onClick={() => props.setServiceType("package")}
                    />
                    <span className="font-weight-bold">Packages</span>
                </div>
            </div>
            <div>
                <NavigationBox forward="/selection" back="rental-type" />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return { currentServiceType: state.rentalType.currentServiceType };
};

export default connect(mapStateToProps, { setServiceType, getServiceType })(
    ServiceTypeTab
);
