import React, { useEffect } from "react";
import { connect } from "react-redux";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { updatePickup } from "../../../actions/index";
import NavigationBox from "../../NavigationBox";

import { checkValid, displayError } from "../../../functions/validation";
import { modifyDate } from "../../../functions/date";

import {
    OrderTextField,
    OrderSelectField,
    OrderTextAreaField,
} from "../OrderFields";

const modifyPickup = (value, pickup, updatePickup) => {
    let itemKey = Object.keys(value)[0];
    let pickupObject = pickup[itemKey];
    let valueObject = value[itemKey];

    let newObject = Object.assign(pickupObject, valueObject);
    let returnObject = { ...pickup, ...newObject };
    updatePickup(returnObject);
};

const PickupTab = (props) => {
    const { order, updatePickup, restrictions, rentalDetails } = props;
    const { pickup } = order;
    const {
        street_address,
        street_address_2,
        city,
        postal_code,
        date,
        time,
        notes,
    } = pickup;

    useEffect(() => {
        modifyPickup({ date: { value: "" } }, pickup, updatePickup);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="pickup">
            <h1>Pickup Details</h1>
            <p>Where will we pick up the RED Boxes</p>
            <div className="orderfield__wrapper">
                <OrderTextField
                    field={street_address}
                    fieldName="street_address"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Street Address*"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                />
                <OrderTextField
                    field={street_address_2}
                    fieldName="street_address_2"
                    validationType="text"
                    restrictions={restrictions}
                    validate={false}
                    placeholder="Unit / Suite #"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                />

                <OrderTextField
                    field={city}
                    fieldName="city"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="City*"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                />

                <OrderTextField
                    field={postal_code}
                    fieldName="postal_code"
                    validationType="postalcode"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Postal Code*"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                />

                <DatePicker
                    className={`orderfield__input ${displayError(date.valid)} `}
                    placeholderText="Pickup Date"
                    selected={date.value}
                    onChange={(v) => {
                        modifyPickup(
                            { date: { value: v } },
                            pickup,
                            updatePickup
                        );
                    }}
                    minDate={modifyDate(
                        new Date(order.delivery.date.value),
                        "add",
                        7 * rentalDetails.currentDuration
                    )}
                    maxDate={modifyDate(
                        new Date(order.delivery.date.value),
                        "add",
                        7 * rentalDetails.currentDuration + 2
                    )}
                    filterDate={(date) => {
                        let day = date.getDay(date);
                        return day !== 0 && day !== 6;
                    }}
                    onBlur={(v) => {
                        console.log(v.target.value);
                        let value = checkValid(v.target.value, "date", "text");
                        modifyPickup(value, pickup, updatePickup);
                    }}
                />
                <OrderSelectField
                    field={time}
                    fieldName="time"
                    validationType="select"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Pickup Time*"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                    additionalClasses="mr-0"
                    options={[
                        {
                            label: "8:00 - 12:00",
                            value: "morning",
                        },
                        {
                            label: "12:00 - 4:00",
                            value: "afternoon",
                        },
                    ]}
                />
                <OrderTextAreaField
                    field={notes}
                    fieldName="notes"
                    validationType="text"
                    restrictions={restrictions}
                    validate={false}
                    placeholder="Pickup Notes"
                    stateObject={pickup}
                    modifyFunction={modifyPickup}
                    updateAction={updatePickup}
                />
            </div>
            <NavigationBox back="/delivery" forward="/billing" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        order: state.order,
        restrictions: state.restrictions,
        rentalDetails: state.rentalDetails,
    };
};

export default connect(mapStateToProps, { updatePickup })(PickupTab);
