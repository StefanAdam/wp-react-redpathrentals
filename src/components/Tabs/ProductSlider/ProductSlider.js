import React from "react";
import { connect } from "react-redux";

import SVG from "react-inlinesvg";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "./ProductSlider.scss";

import Slider from "react-slick";

import isSelected from "../../../functions/isSelected";

import { setCurrentProduct, addItemCart } from "../../../actions";
import { onSale } from "../../../functions/sale";

const ProductSlider = (props) => {
    const { products, currentProduct, slides, addable } = props;

    const productsArray = Object.keys(products).map((i) => products[i]);

    const extRegex = /(?:\.([^.]+))?$/;

    const addToCart = (product) => {
        let array = [];
        array.push(product);
        props.addItemCart(array);
    };

    const outputImageContainer = (product) => {
        const { image: src, name, price } = product;
        return (
            <div className="product__details">
                {outputImage(src, name)}
                {addable && (
                    <div
                        className="overlay"
                        onClick={(e) => {
                            e.stopPropagation();
                        }}
                    >
                        <div className="overlay__content">
                            <div className="overlay__name">{name}</div>
                            <div className="overlay__price">
                                {onSale(product) && (
                                    <span className="overlay__price--regular">
                                        {`$${parseFloat(
                                            product.regularPrice
                                        ).toFixed(2)}
                                        `}
                                    </span>
                                )}
                                {`$${parseFloat(price).toFixed(2)}`}
                            </div>
                            <div
                                className="overlay__button btn btn-primary"
                                onClick={(e) => {
                                    e.stopPropagation();
                                    addToCart(product);
                                }}
                            >
                                +
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    };

    const outputImage = (src, alt) => {
        if (extRegex.exec(src)[1] === "svg") {
            return <SVG src={src} alt={alt} />;
        } else {
            return <img src={src} alt={alt} />;
        }
    };

    return (
        <Slider
            {...{
                arrows: true,
                slidesToShow: parseInt(slides),
            }}
        >
            {productsArray.map((product) => {
                return (
                    <div key={product.id} className="product__wrapper">
                        <div
                            key={product.id}
                            className={`product ${isSelected(
                                currentProduct.slug,
                                product.slug,
                                "selected"
                            )}`}
                        >
                            <div
                                className="product__content"
                                onClick={() => {
                                    props.setCurrentProduct(product);
                                }}
                            >
                                {outputImageContainer(product)}
                                {!addable && <p>{product.name}</p>}
                            </div>
                        </div>
                    </div>
                );
            })}
        </Slider>
    );
};
const mapStateToProps = (state) => {
    return {
        currentProduct: state.rentalDetails.currentProduct,
    };
};

export default connect(mapStateToProps, {
    setCurrentProduct,
    addItemCart,
})(ProductSlider);
