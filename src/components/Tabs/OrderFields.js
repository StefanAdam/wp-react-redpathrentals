import React, { useState, useEffect } from "react";

import { wp } from "../../apis/wordpress";
import { checkValid, displayError } from "../../functions/validation";

import "./OrderFields.scss";

export const OrderTextField = (props) => {
    const {
        field,
        fieldName,
        validationType,
        restrictions,
        validate,
        placeholder,
        stateObject,
        modifyFunction,
        updateAction,
        additionalClasses,
    } = props; // Field is Object from orderfield State

    return (
        <input
            className={`orderfield__input ${displayError(field.valid)} ${
                additionalClasses ? additionalClasses : ""
            } `}
            placeholder={placeholder}
            value={field.value}
            onChange={(v) => {
                modifyFunction(
                    { [fieldName]: { value: v.target.value } },
                    stateObject,
                    updateAction
                );
            }}
            onBlur={(v) => {
                let value = checkValid(
                    v.target.value,
                    fieldName,
                    validationType,
                    restrictions,
                    validate
                );
                modifyFunction(value, stateObject, updateAction);
            }}
        />
    );
};

export const OrderSelectField = (props) => {
    const renderOptions = (options) => {
        return options.map((option, index) => {
            return (
                <option
                    key={index}
                    className="orderfield__option"
                    value={option.value}
                >
                    {option.label}
                </option>
            );
        });
    };

    const {
        field,
        fieldName,
        validationType,
        restrictions,
        validate,
        placeholder,
        stateObject,
        modifyFunction,
        updateAction,
        options,
        additionalClasses,
    } = props;

    return (
        <select
            className={`orderfield__select ${displayError(field.valid)} ${
                additionalClasses ? additionalClasses : ""
            } `}
            value={field.value}
            onChange={(v) => {
                modifyFunction(
                    { [fieldName]: { value: v.target.value } },
                    stateObject,
                    updateAction
                );
            }}
            onBlur={(v) => {
                let value = checkValid(
                    v.target.value,
                    fieldName,
                    validationType,
                    restrictions,
                    validate
                );
                modifyFunction(value, stateObject, updateAction);
            }}
        >
            <option className="orderfield__option" value="select">
                {placeholder}
            </option>
            {renderOptions(options)}
        </select>
    );
};

export const OrderTextAreaField = (props) => {
    const {
        field,
        fieldName,
        validationType,
        restrictions,
        validate,
        placeholder,
        stateObject,
        modifyFunction,
        updateAction,
    } = props;
    return (
        <textarea
            className={`orderfield__textarea ${displayError(field.valid)} `}
            placeholder={placeholder}
            value={field.value}
            onChange={(v) => {
                modifyFunction(
                    { [fieldName]: { value: v.target.value } },
                    stateObject,
                    updateAction
                );
            }}
            onBlur={(v) => {
                let value = checkValid(
                    v.target.value,
                    fieldName,
                    validationType,
                    restrictions,
                    validate
                );
                modifyFunction(value, stateObject, updateAction);
            }}
        ></textarea>
    );
};

export const OrderCheckBoxField = (props) => {
    const {
        field,
        fieldName,
        validationType,
        restrictions,
        validate,
        placeholder,
        stateObject,
        modifyFunction,
        updateAction,
        hasLink,
        linkDestination,
    } = props;

    const [link, setLink] = useState("#");

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const getTermsPage = async (setLink) => {
        const result = await wp.pages().get((err, data) => {});
        let mapResult = result.map((page) => {
            if (page.slug === linkDestination) {
                return page.link;
            }
            return null;
        });
        const filteredResult = mapResult.filter((el) => {
            return el != null;
        });
        console.log(filteredResult[0]);
        setLink(filteredResult[0]);
    };
    useEffect(() => {
        getTermsPage(setLink);
    }, [getTermsPage]);
    return (
        <label
            className={`checkbox_container orderfield__checkbox ${displayError(
                field.valid
            )}`}
            checked={field.value}
            onChange={(v) => {
                modifyFunction(
                    { [fieldName]: { value: v.target.checked } },
                    stateObject,
                    updateAction
                );
                let value = checkValid(
                    v.target.checked,
                    fieldName,
                    validationType,
                    restrictions,
                    validate
                );
                modifyFunction(value, stateObject, updateAction);
            }}
        >
            Agree to{" "}
            {hasLink && (
                <a href={link} rel="noopener noreferrer" target="_blank">
                    {" "}
                    {placeholder}{" "}
                </a>
            )}
            {!hasLink && placeholder}
            <input type="checkbox" />
            <span className="checkmark"></span>
        </label>
    );
};
