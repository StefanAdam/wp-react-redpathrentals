import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import NavigationBox from "../../NavigationBox";

import { setRentalType, getRentalType } from "../../../actions";
import isSelected from "../../../functions/isSelected";

import "./RentalTypeTab.scss";

import { ReactComponent as ResidentialSVG } from "../Assets/residential.svg";
import { ReactComponent as CommercialSVG } from "../Assets/commercial.svg";

const RentalTypeTab = (props) => {
    const [selected, setSelected] = useState(props.currentRentalType);

    useEffect(() => {
        setSelected(props.currentRentalType);
    }, [props.currentRentalType]);

    return (
        <div className="d-flex flex-column align-items-center">
            <h1>Type of Rental</h1>
            <p>Is this rental for residential or Commercial?</p>
            <div className="two-panel d-flex justify-content-center">
                <div
                    className={`residential service d-flex flex-column align-items-center ${isSelected(
                        selected,
                        "residential",
                        "selected"
                    )}`}
                >
                    <ResidentialSVG
                        onClick={() => props.setRentalType("residential")}
                    />
                    <span className="font-weight-bold">Residential</span>
                </div>
                <div
                    className={`commercial service d-flex flex-column align-items-center ${isSelected(
                        selected,
                        "commercial",
                        "selected"
                    )}`}
                >
                    <CommercialSVG
                        onClick={() => props.setRentalType("commercial")}
                    />
                    <span className="font-weight-bold">Commercial</span>
                </div>
            </div>
            <div>
                <NavigationBox type="forward" forward="/service-type" />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return { currentRentalType: state.rentalType.currentRentalType };
};

export default connect(mapStateToProps, { setRentalType, getRentalType })(
    RentalTypeTab
);
