import React from "react";
import { connect } from "react-redux";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { updateDelivery } from "../../../actions/index";
import NavigationBox from "../../NavigationBox";

import { checkValid, displayError } from "../../../functions/validation";
import { modifyDate } from "../../../functions/date";

import {
    OrderTextField,
    OrderSelectField,
    OrderTextAreaField,
} from "../OrderFields";

const modifyDelivery = (value, delivery, updateDelivery) => {
    let itemKey = Object.keys(value)[0];
    let deliveryObject = delivery[itemKey];
    let valueObject = value[itemKey];
    let newObject = Object.assign(deliveryObject, valueObject);
    let returnObject = { ...delivery, ...newObject };
    updateDelivery(returnObject);
};

const DeliveryTab = (props) => {
    const { order, updateDelivery, restrictions } = props;
    const { delivery } = order;
    const {
        street_address,
        street_address_2,
        city,
        postal_code,
        date,
        time,
        notes,
    } = delivery;
    return (
        <div className="delivery">
            <h1>Delivery Details</h1>
            <p>Where will we drop off the RED Boxes</p>
            <div className="orderfield__wrapper">
                <OrderTextField
                    field={street_address}
                    fieldName="street_address"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Street Address*"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                />
                <OrderTextField
                    field={street_address_2}
                    fieldName="street_address_2"
                    validationType="text"
                    restrictions={restrictions}
                    validate={false}
                    placeholder="Unit / Suite #"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                />

                <OrderTextField
                    field={city}
                    fieldName="city"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="City*"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                />

                <OrderTextField
                    field={postal_code}
                    fieldName="postal_code"
                    validationType="postalcode"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Postal Code*"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                />

                <DatePicker
                    className={`orderfield__input ${displayError(date.valid)} `}
                    placeholderText="Delivery Date"
                    selected={date.value}
                    onChange={(v) => {
                        modifyDelivery(
                            { date: { value: v } },
                            delivery,
                            updateDelivery
                        );
                    }}
                    minDate={modifyDate(new Date(), "add", 2)}
                    maxDate={modifyDate(new Date(), "add", 91)}
                    filterDate={(date) => {
                        let day = date.getDay(date);
                        return day !== 0 && day !== 6;
                    }}
                    onBlur={(v) => {
                        console.log(v.target.value);
                        let value = checkValid(v.target.value, "date", "text");
                        modifyDelivery(value, delivery, updateDelivery);
                    }}
                />
                <OrderSelectField
                    field={time}
                    fieldName="time"
                    validationType="select"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Delivery Time*"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                    additionalClasses="mr-0"
                    options={[
                        {
                            label: "8:00 - 12:00",
                            value: "morning",
                        },
                        {
                            label: "12:00 - 4:00",
                            value: "afternoon",
                        },
                    ]}
                />
                <OrderTextAreaField
                    field={notes}
                    fieldName="notes"
                    validationType="text"
                    restrictions={restrictions}
                    validate={false}
                    placeholder="Delivery Notes"
                    stateObject={delivery}
                    modifyFunction={modifyDelivery}
                    updateAction={updateDelivery}
                />
            </div>
            <NavigationBox back="/addons" forward="/pickup" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return { order: state.order, restrictions: state.restrictions };
};

export default connect(mapStateToProps, { updateDelivery })(DeliveryTab);
