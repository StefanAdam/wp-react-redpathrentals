import React, { useEffect } from "react";
import { connect } from "react-redux";

import { setTaxes, setOrderTax, setOrderTotal } from "../../../actions";

import NavigationBox from "../../NavigationBox";
import AddonsCart from "../AddonsTab/AddonsCart";
import SelectionSummary from "../SelectionTab/SelectionSummary";
import WP_Headless_Fetch_Taxes from "../../../functions/WP_Headless_Fetch_Taxes";

const ReviewTab = (props) => {
    const {
        cart,
        order,
        rentalDetails,
        setTaxes,
        taxes,
        setOrderTax,
        setOrderTotal,
    } = props;

    const applyTaxes = () => {
        let appliedTaxes = [];
        taxes.map((tax) => {
            if (tax.country === order.delivery.country.value) {
                if (
                    tax.state === "" ||
                    tax.state === order.delivery.province.value
                ) {
                    appliedTaxes.push(tax);
                }
            }
        });
        console.log(appliedTaxes);
        appliedTaxes.map((tax) => {
            let taxTotal = order.total * (tax.rate / 100);
            console.log(taxTotal);
        });
    };

    useEffect(() => {
        WP_Headless_Fetch_Taxes(setTaxes);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setOrderTotal(
            parseFloat(
                parseFloat(rentalDetails.currentPrice) + parseFloat(cart.total)
            ).toFixed(2)
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="review">
            <h1>Review Order</h1>
            <p>Review and Confirm Details of your Order</p>
            <h2>Product</h2>
            <SelectionSummary rentalDetails={rentalDetails} text={""} image />
            <h2>Addons</h2>
            <AddonsCart />

            <h2>Total</h2>
            <p>
                Package Total:{" "}
                {parseFloat(rentalDetails.currentPrice).toFixed(2)}
            </p>
            <p>Addons Total: {cart.total} </p>
            <p>Subtotal: {order.total}</p>
            <p> Taxes: {applyTaxes()}</p>
            <h2>Choose Payment Method</h2>
            <NavigationBox type="back" back="/billing" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        order: state.order,
        rentalDetails: state.rentalDetails,
        taxes: state.taxes,
    };
};

export default connect(mapStateToProps, {
    setTaxes,
    setOrderTax,
    setOrderTotal,
})(ReviewTab);
