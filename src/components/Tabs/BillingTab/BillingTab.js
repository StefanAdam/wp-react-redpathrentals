import React from "react";
import { connect } from "react-redux";

import { updateBilling } from "../../../actions/index";
import NavigationBox from "../../NavigationBox";

import {
    OrderTextField,
    OrderSelectField,
    OrderCheckBoxField,
} from "../OrderFields";

const modifyBilling = (value, billing, updateBilling) => {
    let itemKey = Object.keys(value)[0];
    let billingObject = billing[itemKey];
    let valueObject = value[itemKey];

    let newObject = Object.assign(billingObject, valueObject);
    let returnObject = { ...billing, ...newObject };
    updateBilling(returnObject);
};

const BillingTab = (props) => {
    const { order, updateBilling, restrictions } = props;
    const { billing } = order;
    const {
        first_name,
        last_name,
        country,
        street_address,
        street_address_2,
        city,
        postal_code,
        province,
        phone,
        email,
        terms,
    } = billing;

    return (
        <div className="billing">
            <h1>Billing Details</h1>
            <p>Provide your Billing Details</p>
            <div className="orderfield__wrapper">
                <OrderTextField
                    field={first_name}
                    fieldName="first_name"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="First Name*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderTextField
                    field={last_name}
                    fieldName="last_name"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Last Name*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderSelectField
                    field={country}
                    fieldName="country"
                    validationType="select"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Country"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                    options={[
                        {
                            label: "Canada",
                            value: "CA",
                        },
                    ]}
                />
                <OrderTextField
                    field={street_address}
                    fieldName="street_address"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Street Address*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderTextField
                    field={street_address_2}
                    fieldName="street_address_2"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Unit / Suite #"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderTextField
                    field={postal_code}
                    fieldName="postal_code"
                    validationType="postalcode"
                    restrictions={restrictions}
                    validate={false}
                    placeholder="Postal Code*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderTextField
                    field={city}
                    fieldName="city"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="City*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderSelectField
                    field={province}
                    fieldName="province"
                    validationType="select"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Province*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                    options={[...restrictions.validProvinces]}
                />
                <OrderTextField
                    field={phone}
                    fieldName="phone"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Phone*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderTextField
                    field={email}
                    fieldName="email"
                    validationType="text"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Email*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                />
                <OrderCheckBoxField
                    field={terms}
                    fieldName="terms"
                    validationType="checkbox"
                    restrictions={restrictions}
                    validate={true}
                    placeholder="Terms of Service*"
                    stateObject={billing}
                    modifyFunction={modifyBilling}
                    updateAction={updateBilling}
                    hasLink
                    linkDestination="terms-conditions"
                />
            </div>
            <NavigationBox back="/pickup" forward="/review" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        order: state.order,
        restrictions: state.restrictions,
    };
};

export default connect(mapStateToProps, { updateBilling })(BillingTab);
