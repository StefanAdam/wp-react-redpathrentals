import React, { useEffect } from "react";
import { connect } from "react-redux";

import {
    addItemCart,
    updateItemCart,
    removeItemCart,
    setTotal,
} from "../../../actions";

import { modifyTotals } from "../../../functions/price";

import CouponsTable from "../CouponsTable/CouponsTable";

const updateQuantity = (item, value, updateItemCart) => {
    let array = [item];
    updateItemCart(array, value);
};

const countTotals = (items, setTotal) => {
    let total = [0];

    items.forEach((item) => {
        let itemTotal = parseFloat(item.price) * parseFloat(item.count);
        total.push(itemTotal);
    });

    let combinedTotal = total.reduce((a, b) => a + b, 0).toFixed(2);
    setTotal(combinedTotal);
};

const AddonsCart = (props) => {
    const { cart, removeItemCart, updateItemCart, setTotal } = props;

    useEffect(() => {
        countTotals(cart.items, setTotal);
    }, [cart.items, setTotal]);

    return (
        <>
            <table>
                <thead>
                    <tr>
                        <th className="sr-only">Remove</th>
                        <th className="sr-only">Thumbnail</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                </thead>

                <tbody>
                    {cart.items.map((item, index) => {
                        return (
                            <tr key={item.id}>
                                <td className="remove">
                                    <div
                                        className="btn btn-primary"
                                        onClick={(e) => {
                                            removeItemCart([item]);
                                        }}
                                    >
                                        {" "}
                                        X{" "}
                                    </div>
                                </td>
                                <td className="thumbnail">
                                    <img
                                        src={item.image}
                                        alt={item.name}
                                        height="50px"
                                        width="50px"
                                    />
                                </td>
                                <td className="product">{item.name}</td>
                                <td className="price">{`$${parseFloat(
                                    item.price
                                ).toFixed(2)}`}</td>
                                <td className="quantity">
                                    <input
                                        type="number"
                                        value={item.count}
                                        onChange={(e) => {
                                            updateQuantity(
                                                item,
                                                e.target.value,
                                                updateItemCart
                                            );
                                        }}
                                    />{" "}
                                </td>
                                <td className="total">
                                    {`$${parseFloat(
                                        item.price * parseInt(item.count)
                                    ).toFixed(2)}`}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
                <tfoot>
                    <tr>
                        <td>Total</td>
                        <td>{`$${modifyTotals(cart)}`} </td>
                    </tr>
                    <tr>
                        <td>Coupons Applied:</td>
                        <td>{cart.coupons.code}</td>
                        <td>
                            You Saved:
                            {`$${parseFloat(
                                cart.total - modifyTotals(cart)
                            ).toFixed(2)}`}
                        </td>
                    </tr>
                </tfoot>
            </table>
            <CouponsTable />
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    };
};

export default connect(mapStateToProps, {
    addItemCart,
    updateItemCart,
    removeItemCart,
    setTotal,
})(AddonsCart);
