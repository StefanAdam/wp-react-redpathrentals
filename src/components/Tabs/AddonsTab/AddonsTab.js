import React, { useEffect } from "react";
import { connect } from "react-redux";

import { setProducts } from "../../../actions";

import ProductSlider from "../ProductSlider/ProductSlider";
import AddonsCart from "./AddonsCart";
import NavigationBox from "../../NavigationBox";

import WP_Headless_Fetch_Products from "../../../functions/WP_Headless_Fetch_Products";
import WP_Headless_Fetch_PaymentGateways from "../../../functions/WP_Headless_Fetch_PaymentGateways";

const AddonsTab = (props) => {
    const { setProducts } = props;

    // Only Runs once when Component Mounts // Effectively ComponentDidMount
    useEffect(() => {
        WP_Headless_Fetch_Products(setProducts, 100, 27);
    }, [setProducts]);

    useEffect(() => {
        WP_Headless_Fetch_PaymentGateways();
    }, []);

    return (
        <div>
            <h1> Choose Your Addons </h1>
            <ProductSlider products={props.products} slides="2" addable />
            <AddonsCart />
            <NavigationBox back="/selection" forward="/delivery" />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.products,
    };
};

export default connect(mapStateToProps, {
    setProducts,
})(AddonsTab);
