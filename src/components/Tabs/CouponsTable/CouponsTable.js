import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import WP_Headless_Fetch_Coupons from "../../../functions/WP_Headless_Fetch_Coupons";
import { setCoupons, applyCoupon, removeCoupon } from "../../../actions";

import { activateCoupon } from "../../../functions/coupons";

const updateState = (stateUpdater, value) => {
    stateUpdater(value);
};

const CouponsTable = (props) => {
    const { applyCoupon, setCoupons, coupons, cart } = props;
    const [couponValue, setCouponValue] = useState("");

    useEffect(() => {
        WP_Headless_Fetch_Coupons(setCoupons, 100);
    }, [setCoupons]);
    return (
        <table>
            <thead>
                <tr>
                    <td className="coupon">
                        <div className="coupon__controls">
                            Add Coupon:{" "}
                            <input
                                value={props.couponInputValue}
                                onChange={(v) => {
                                    updateState(setCouponValue, v.target.value);
                                }}
                            />
                            <div
                                className="btn btn-primary coupon__submit"
                                onClick={() => {
                                    activateCoupon(
                                        couponValue,
                                        coupons,
                                        cart,
                                        applyCoupon
                                    );
                                }}
                            >
                                Submit
                            </div>
                            <div></div>
                        </div>
                    </td>
                </tr>
            </thead>
        </table>
    );
};

const mapStateToProps = (state) => {
    return {
        coupons: state.coupons,
        applied_coupons: state.applied_coupons,
        cart: state.cart,
    };
};

export default connect(mapStateToProps, {
    applyCoupon,
    removeCoupon,
    setCoupons,
})(CouponsTable);
