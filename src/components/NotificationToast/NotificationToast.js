import { ToastsStore } from "@vashnak/react-toasts";

import "./NotificationToast.scss";

const NotificationToast = (message, type) => {
    switch (type) {
        case "info":
            ToastsStore.info(message);
            break;
        case "sucess":
            ToastsStore.success(message);
            break;
        case "warning":
            ToastsStore.warning(message);
            break;
        case "error":
            ToastsStore.error(message);
            break;
        default:
            ToastsStore.info(message);
    }
};

export default NotificationToast;
