import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { setCurrentPage } from "../actions";

import "./NavigationBox.scss";

const NavigationBox = (props) => {
    let buttonsEnabled = { value: true };
    Object.keys(props.order).forEach((category) => {
        Object.keys(props.order[category]).forEach((item) => {
            if (props.order[category][item]) {
                if (props.order[category][item].valid === false) {
                    buttonsEnabled.value = false;
                }
            }
        });
    });

    const renderButtons = () => {
        switch (props.type) {
            case "back":
                return (
                    <Link
                        to={props.back}
                        className={`btn btn-primary ${
                            buttonsEnabled.value ? "" : "disabled"
                        }`}
                        onClick={() =>
                            props.setCurrentPage(props.back.substr(1))
                        }
                    >
                        Back
                    </Link>
                );
            case "forward":
                return (
                    <Link
                        to={props.forward}
                        className={`btn btn-primary ${
                            buttonsEnabled.value ? "" : "disabled"
                        }`}
                        onClick={() =>
                            props.setCurrentPage(props.forward.substr(1))
                        }
                    >
                        Next
                    </Link>
                );
            default:
                return (
                    <React.Fragment>
                        <Link
                            to={props.back}
                            className={`btn btn-primary ${
                                buttonsEnabled.value ? "" : "disabled"
                            }`}
                            onClick={() =>
                                props.setCurrentPage(props.back.substr(1))
                            }
                        >
                            Back
                        </Link>
                        <Link
                            to={props.forward}
                            className={`btn btn-primary ${
                                buttonsEnabled.value ? "" : "disabled"
                            }`}
                            onClick={() =>
                                props.setCurrentPage(props.forward.substr(1))
                            }
                        >
                            Next
                        </Link>
                    </React.Fragment>
                );
        }
    };
    return <div className="navigation-box">{renderButtons()}</div>;
};

const mapStateToProps = (state) => {
    return {
        order: state.order,
    };
};

export default connect(mapStateToProps, { setCurrentPage })(NavigationBox);
