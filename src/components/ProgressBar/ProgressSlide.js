import React from "react";

const ProgressSlide = props => {
	return (
		<li
			key={props.index}
			className={`progress-slide ${props.slide.id} ${props.active}`}
			onClick={() => {
				props.onSlideClick(props.slide.id);
			}}
		>
			{props.slide.text}
		</li>
	);
};

export default ProgressSlide;
