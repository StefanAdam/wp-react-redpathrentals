import React, { useEffect } from "react";

const ProgressIcon = (props) => {
    useEffect(() => {
        let item;
        if (props.selectedSlide !== "") {
            item = document.querySelector(`.${props.selectedSlide}`);
        } else {
            item = document.querySelector(".rental-type");
        }
        const icon = document.querySelector(".progress-icon");
        const position = item.offsetLeft + item.offsetWidth / 2 - 12.5;
        icon.style.left = `${position}px`;
    });
    return <div className="progress-icon"></div>;
};

export default ProgressIcon;
