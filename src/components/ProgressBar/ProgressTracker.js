import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { setCurrentPage } from "../../actions";

import history from "../../history";

import ProgressSlide from "./ProgressSlide";
import ProgressIcon from "./ProgressIcon";
import isSelected from "../../functions/isSelected";

import "./ProgressTracker.scss";

const ProgressTracker = (props) => {
    const [selected, setSelected] = useState(props.currentPage);

    const updateSelected = (item) => {
        props.setCurrentPage(item);
    };

    useEffect(
        (props) => {
            setSelected(history.location.pathname.substr(1));
            setCurrentPage(history.location.pathname.substr(1));
        },
        [props.currentPage]
    );

    const progressSlides = [
        { id: "rental-type", text: "Rental Type" },
        { id: "service-type", text: "Service Type" },
        { id: "selection", text: "RED Box Selection" },
        { id: "addons", text: "Add-On's" },
        { id: "delivery", text: "Delivery" },
        { id: "pickup", text: "Pickup" },
        { id: "billing", text: "Billing" },
        { id: "review", text: "Review" },
    ];

    const renderSlides = () => {
        return progressSlides.map((slide, index) => {
            return (
                <ProgressSlide
                    key={index}
                    slide={slide}
                    active={isSelected(selected, slide.id)}
                    onSlideClick={updateSelected}
                />
            );
        });
    };
    return (
        <div className="progress-tracker py-4 py-md-4 mt-5">
            <ul className="progress-slider d-flex nav nav-pills">
                {renderSlides()}
            </ul>
            <ProgressIcon selectedSlide={selected} />
        </div>
    );
};

const mapStateToProps = (state) => {
    return { currentPage: state.page.currentPage };
};

export default connect(mapStateToProps, { setCurrentPage })(ProgressTracker);
